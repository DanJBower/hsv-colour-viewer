# HSV Colour Viewer #
### What is this repository for? ###
This repository contains the source code for a relatively simple application that allows for you to see how OpenCV can filter out different colours from a live camera feed

### How do I get set up? ###
#### Build and Deploy ###
To get ready

* Install Android Studio (Version 3.0.1 or greater)
* Make sure you have Android SDK up to date, with NDK installed and CMake
* Clone this repository
* Open the project in Android Studio and sync the gradle

To deploy this to your phone

* Plug your phone into your PC
* Click Run on Android Studio
* Select your device and click "OK" to deploy the app to your mobile
	* If you have any issues take a look at the `Run on a real device` section of this [Android Developer Documentation Page](https://developer.android.com/training/basics/firstapp/running-app.html#RealDevice)

#### Deploy Pre Built Application ####
*Add to downloads sections and mention abd deploy*