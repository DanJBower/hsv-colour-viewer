#include <jni.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <vector>

extern "C" {
    JNIEXPORT void JNICALL Java_com_cerbyarms_cerbyarms_hsvcolorviewer_HSVImageViewer_DisplayConvertColour(JNIEnv*, jobject, jlong mask) {
        cv::Mat& mRgbmask = *(cv::Mat*)mask;
        //Convert colour from BGR to HSV
        cvtColor(mRgbmask, mRgbmask, cv::COLOR_BGR2HSV);
    }

    JNIEXPORT void JNICALL Java_com_cerbyarms_cerbyarms_hsvcolorviewer_HSVImageViewer_DisplayInRange(JNIEnv*, jobject, jlong mask,
            jint minHue, jint maxHue, jint minSat, jint maxSat, jint minVal, jint maxVal) {
        cv::Mat& mRgbmask = *(cv::Mat*)mask;

        //Creates a mask of colours in range specified
        cv::inRange(mRgbmask, cv::Scalar(minHue, minSat, minVal), cv::Scalar(maxHue, maxSat, maxVal), mRgbmask);
    }

    JNIEXPORT void JNICALL Java_com_cerbyarms_cerbyarms_hsvcolorviewer_HSVImageViewer_DisplayHSVColorMask(JNIEnv*, jobject, jlong addrRgba, jlong mask) {
        cv::Mat& mRgb = *(cv::Mat*)addrRgba;
        cv::Mat& mMask = *(cv::Mat*)mask;

        //Convert the colour from BGR to HSV
        cvtColor(mRgb, mRgb, cv::COLOR_BGR2HSV);

        //Apply colour to the mask
        mRgb.copyTo(mMask, mMask);
    }

    JNIEXPORT void JNICALL Java_com_cerbyarms_cerbyarms_hsvcolorviewer_HSVImageViewer_DisplayBGRColorMask(JNIEnv*, jobject, jlong addrRgba, jlong mask) {
        cv::Mat& mRgb = *(cv::Mat*)addrRgba;
        cv::Mat& mMask = *(cv::Mat*)mask;

        //Apply colour to the mask
        mRgb.copyTo(mMask, mMask);
    }
}