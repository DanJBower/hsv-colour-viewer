package com.cerbyarms.cerbyarms.hsvcolorviewer;

import android.Manifest;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.TextView;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class HSVImageViewer extends AppCompatActivity {
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 1;
    private CameraBridgeViewBase mOpenCvCameraView;
    private Resources res;
    private SeekBar minHue, maxHue, minSat, maxSat, minVal, maxVal;
    private CheckBox convCol, inRange, bgrColorMask, hsvColorMask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hsvimage_viewer);
        res = getResources();

        linkSeekBars();
        linkCheckBoxes();

        mOpenCvCameraView = findViewById(R.id.HelloOpenCvView);
        mOpenCvCameraView.setCvCameraViewListener(new CameraBridgeViewBase.CvCameraViewListener2() {
            Mat mRgba;
            Mat mask;
            @Override
            public void onCameraViewStarted(int width, int height) {
                mask = new Mat();
            }

            @Override
            public void onCameraViewStopped() {
                mRgba.release();
                mask.release();
                System.gc();
            }

            @Override
            public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
                mRgba = inputFrame.rgba();
                mRgba.copyTo(mask);

                if(convCol.isChecked())
                    DisplayConvertColour(mask.getNativeObjAddr());

                if(inRange.isChecked())
                    DisplayInRange(mask.getNativeObjAddr(), minHue.getProgress(), maxHue.getProgress(), minSat.getProgress(), maxSat.getProgress(), minVal.getProgress(), maxVal.getProgress());

                if(hsvColorMask.isChecked())
                    DisplayHSVColorMask(mRgba.getNativeObjAddr(), mask.getNativeObjAddr());

                if(bgrColorMask.isChecked())
                    DisplayBGRColorMask(mRgba.getNativeObjAddr(), mask.getNativeObjAddr());

                return mask;
            }
        });
    }

    @Override
    public void onResume()
    {
        super.onResume();

        //Request camera permission
        if(ContextCompat.checkSelfPermission(HSVImageViewer.this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
        }

        //Initiate OpenCV
        if (!OpenCVLoader.initDebug()) {
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_4_0, this, mLoaderCallback);
        } else {
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    //Handle permission requests
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //Handle camera permission request
        if(requestCode == MY_PERMISSIONS_REQUEST_CAMERA) {
            if(!(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
            }
        }
    }

    //Handle OpenCV Loading
    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        //When open cv has loaded
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    //Load other native libraries
                    System.loadLibrary("native-lib");

                    //Start camera preview
                    mOpenCvCameraView.enableView();
                } break;
                default:
                    super.onManagerConnected(status);
                break;
            }
        }
    };

    //Free up camera when app stops
    @Override
    protected void onPause() {
        if(mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
        super.onPause();
    }

    //Stops the UI from creating values that don't work with each other
    private void linkSeekBars() {
        minHue = findViewById(R.id.min_hue_value);
        TextView minHueValue = findViewById(R.id.min_hue_value_text);

        maxHue = findViewById(R.id.max_hue_value);
        TextView maxHueValue = findViewById(R.id.max_hue_value_text);

        minSat = findViewById(R.id.min_sat_value);
        TextView minSatValue = findViewById(R.id.min_sat_value_text);

        maxSat = findViewById(R.id.max_sat_value);
        TextView maxSatValue = findViewById(R.id.max_sat_value_text);

        minVal = findViewById(R.id.min_val_value);
        TextView minValValue = findViewById(R.id.min_val_value_text);

        maxVal = findViewById(R.id.max_val_value);
        TextView maxValValue = findViewById(R.id.max_val_value_text);

        minHue.setOnSeekBarChangeListener(new LinkSeekBarsListener(minHueValue, maxHue, false));
        maxHue.setOnSeekBarChangeListener(new LinkSeekBarsListener(maxHueValue, minHue, true));
        minSat.setOnSeekBarChangeListener(new LinkSeekBarsListener(minSatValue, maxSat, false));
        maxSat.setOnSeekBarChangeListener(new LinkSeekBarsListener(maxSatValue, minSat, true));
        minVal.setOnSeekBarChangeListener(new LinkSeekBarsListener(minValValue, maxVal, false));
        maxVal.setOnSeekBarChangeListener(new LinkSeekBarsListener(maxValValue, minVal, true));

        minHueValue.setText(res.getString(R.string.value, String.format(Locale.ENGLISH ,"%03d", minHue.getProgress())));
        maxHueValue.setText(res.getString(R.string.value, String.format(Locale.ENGLISH ,"%03d", maxHue.getProgress())));
        minSatValue.setText(res.getString(R.string.value, String.format(Locale.ENGLISH ,"%03d", minSat.getProgress())));
        maxSatValue.setText(res.getString(R.string.value, String.format(Locale.ENGLISH ,"%03d", maxSat.getProgress())));
        minValValue.setText(res.getString(R.string.value, String.format(Locale.ENGLISH ,"%03d", minVal.getProgress())));
        maxValValue.setText(res.getString(R.string.value, String.format(Locale.ENGLISH ,"%03d", maxVal.getProgress())));
    }

    //Links together a min and max slider
    //When values change, it updates the value shown in the textview and it prevents a min being greater than a max and vice versa
    private class LinkSeekBarsListener implements SeekBar.OnSeekBarChangeListener {
        private TextView valueDisplay;
        private SeekBar linkedSeekBar;
        private boolean isMax;
        private Resources res;

        LinkSeekBarsListener(TextView valueDisplay, SeekBar linkedSeekBar, boolean isMax) {
            this.valueDisplay = valueDisplay;
            this.linkedSeekBar = linkedSeekBar;
            this.isMax = isMax;
            res = getResources();
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            //Updates text in specific format
            valueDisplay.setText(res.getString(R.string.value, String.format(Locale.ENGLISH ,"%03d", progress)));
            if(isMax)
                maxProgressChange(progress);
            else
                minProgressChange(progress);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }

        //Stops min seek bar being greater than a max seek bar
        private void minProgressChange(int progress) {
            if(progress > linkedSeekBar.getProgress()) {
                linkedSeekBar.setProgress(progress);
            }
        }

        //Stops max seek bar being less than a min seek bar
        private void maxProgressChange(int progress) {
            if(progress < linkedSeekBar.getProgress()) {
                linkedSeekBar.setProgress(progress);
            }
        }
    }

    private void linkCheckBoxes() {
        convCol = findViewById(R.id.conv_col_chk);
        inRange = findViewById(R.id.inrange_chk);
        bgrColorMask = findViewById(R.id.color_chk_bgr);
        hsvColorMask = findViewById(R.id.color_chk_hsv);

        List<CheckBox> checkBoxs = new ArrayList<>();
        checkBoxs.add(convCol);
        checkBoxs.add(inRange);
        checkBoxs.add(hsvColorMask);
        checkBoxs.add(bgrColorMask);

        //Disables all but first checkbox
        disableCheckBoxAfter(checkBoxs, 0);

        convCol.setOnCheckedChangeListener(new CheckBoxValueChangedListener(checkBoxs, 0, false));
        inRange.setOnCheckedChangeListener(new CheckBoxValueChangedListener(checkBoxs, 1, true));
        bgrColorMask.setOnCheckedChangeListener(new LinkCheckBoxValueChangedListener(hsvColorMask, inRange));
        hsvColorMask.setOnCheckedChangeListener(new LinkCheckBoxValueChangedListener(bgrColorMask, inRange));
    }

    //Disables all check boxes after index
    private void disableCheckBoxAfter(List<CheckBox> list, int index) {
        for(int i = index + 1; i < list.size(); i++) {
            list.get(i).setChecked(false);
            list.get(i).setEnabled(false);
        }
    }

    //Dynamically enables or disables checkbox based on state of other checkboxes
    private class CheckBoxValueChangedListener implements CompoundButton.OnCheckedChangeListener {
        private List<CheckBox> list;
        private int index;
        private boolean enableNextTwo;

        CheckBoxValueChangedListener(List<CheckBox> list, int index, boolean enableNextTwo) {
            this.list = list;
            this.index = index;
            this.enableNextTwo = enableNextTwo;
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(isChecked)
                enableNext();
            else
                disableCheckBoxAfter();
        }

        private void enableNext() {
            if(index + (enableNextTwo ? 2 : 1) != list.size()) {
                for(int i = index + 1;  i <= index + (enableNextTwo ? 2 : 1); i++) {
                    list.get(i).setEnabled(true);
                }
            }
        }

        private void disableCheckBoxAfter() {
            for(int i = index + 1; i < list.size(); i++) {
                list.get(i).setChecked(false);
                list.get(i).setEnabled(false);
            }
        }
    }

    //Limits it so only one colour mask can be applied
    private class LinkCheckBoxValueChangedListener implements CompoundButton.OnCheckedChangeListener {
        private CheckBox link;
        private CheckBox beforeThe2;

        LinkCheckBoxValueChangedListener(CheckBox link, CheckBox beforeThe2) {
            this.link = link;
            this.beforeThe2 = beforeThe2;
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(!beforeThe2.isChecked() || isChecked) {
                link.setChecked(false);
                link.setEnabled(false);
            } else {
                link.setEnabled(true);
            }
        }
    }

    //Native method declarations
    public native void DisplayConvertColour(long maskAddr);
    public native void DisplayInRange(long maskAddr, int minHue, int maxHue, int minSat, int maxSat, int minVal, int maxVal);
    public native void DisplayHSVColorMask(long matAddrRgba, long mask);
    public native void DisplayBGRColorMask(long matAddrRgba, long mask);
}
